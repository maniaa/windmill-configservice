
# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

RUN mkdir -pv /opt/mirrorfly/config

# Add a volume pointing to /tmp
VOLUME /tmp


# Make port 8080 available to the world outside this container
EXPOSE 9090

# The application's jar file
ARG JAR_FILE=target/configservice.jar

# Add the application's jar to the container
ADD ${JAR_FILE} configservice.jar

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/configservice.jar"]
